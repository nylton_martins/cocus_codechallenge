package carlos.martins.codewars

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import carlos.martins.codewars.search.SearchActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SearchActivityUITest {

    @JvmField
    @Rule
    val testRule = ActivityTestRule<SearchActivity>(SearchActivity::class.java)

    @Test
    fun checkSearchButton() {
        onView(withId(R.id.searchText))
            .perform(typeText("Unnamed"))
        onView(withId(R.id.searchButton))
            .perform(click())
        onView(withId(R.id.progressLayout))
            .check(matches(isDisplayed()))
    }
}