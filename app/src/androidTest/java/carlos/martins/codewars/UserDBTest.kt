package carlos.martins.codewars

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import carlos.martins.codewars.search.model.User
import carlos.martins.codewars.search.persistence.UserDao
import carlos.martins.codewars.search.persistence.UserDatabase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import org.junit.Rule
import java.util.*

@RunWith(AndroidJUnit4::class)
class UserDBTest {
    private lateinit var userDao: UserDao
    private lateinit var db: UserDatabase

    @get:Rule var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, UserDatabase::class.java)
            .allowMainThreadQueries().build()
        userDao = db.userDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun getUsersWhenDBEmpty() {
        userDao.getUserByIdOrUsername("teste")
            .test()
            .assertNoValues()
    }

    @Test
    fun writeUserAndReadInList() {
        val user = User("teste", "teste", emptyList(), Date(), null)
        userDao.insertUser(user).blockingAwait()

        userDao.getUserByIdOrUsername(user.username)
            .test()
            .assertValue {
                it.username == user.username
            }
    }
}