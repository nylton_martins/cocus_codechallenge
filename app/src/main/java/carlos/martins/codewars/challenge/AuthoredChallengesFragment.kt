package carlos.martins.codewars.challenge

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import carlos.martins.codewars.R
import carlos.martins.codewars.challenge.adapter.AuthoredChallengesListAdapter
import carlos.martins.codewars.challenge.adapter.OnChallengeClickListener
import carlos.martins.codewars.challenge.model.AuthoredChallenge
import carlos.martins.codewars.challenge.service.ChallengesService
import carlos.martins.codewars.challengedetails.ChallengeDetailsActivity
import carlos.martins.codewars.challengedetails.model.ChallengeDetails
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_challenges.*

/**
 * Authored Challenges Fragment.
 *
 * @property presenter presenter to communicate between the model and view.
 * @property username user's username.
 * @property challengesList recycler view with challenges list.
 * @property listOfChallenge list of challenges.
 */
class AuthoredChallengesFragment : Fragment(), ChallengesView, OnChallengeClickListener {

    /**
     * Presenter to communicate between the model and view.
     */
    private val presenter : ChallengesPresenter = ChallengesPresenterImpl(this, ChallengesInteractorImpl(ChallengesService.create()))
    /**
     * User's username.
     */
    private lateinit var username: String
    /**
     * Recycler view with challenges list.
     */
    private lateinit var challengesList: RecyclerView
    /**
     * List of challenges.
     */
    private var listOfChallenge = ArrayList<AuthoredChallenge>()

    companion object {

        /**
         * Function that creates a new instance of the fragment
         *
         * @return the Authored Challenges fragment
         */
        fun newInstance(username : String): AuthoredChallengesFragment {
            val authoredChallengesFragment = AuthoredChallengesFragment()

            val args = Bundle()
            args.putSerializable("username", username)
            authoredChallengesFragment.arguments = args

            return authoredChallengesFragment
        }
    }

    /**
     * Function that retrieves the information from the bundle
     *
     * @param savedInstanceState the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        username = arguments?.getSerializable("username") as String
    }

    /**
     * Function that inflates the layout
     *
     * @param inflater used to inflate the layout
     * @param container parent layout
     * @param savedInstanceState the saved instance state
     * @return the inflated view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_challenges, container, false)
    }

    /**
     * Function invoked when view is created.
     *
     * @param view created view
     * @param savedInstanceState the saved instance state
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(activity)
        challengesList = view.findViewById(R.id.challengesList)
        challengesList.layoutManager = layoutManager
        val adapter = AuthoredChallengesListAdapter(listOfChallenge, this, activity as Context)
        challengesList.adapter = adapter
        presenter.getAuthoredChallenges(username)
    }

    /**
     * Function that shows the authored challenges.
     *
     * @param authoredChallenges list of authored challenges.
     */
    override fun showAuthoredChallenges(authoredChallenges: List<AuthoredChallenge>) {
        if(authoredChallenges.isEmpty()) {
            Snackbar.make(rootView, "User has no Authored Challenges.", Snackbar.LENGTH_LONG).show()
        }
        listOfChallenge.addAll(authoredChallenges)
        challengesList.adapter?.notifyDataSetChanged()
    }

    /**
     * Shows loading progress.
     */
    override fun showLoading() {
        progressLayout.visibility = View.VISIBLE
        progressBar.isIndeterminate = true
    }

    /**
     * Hides loading progress.
     */
    override fun hideLoading() {
        progressLayout.visibility = View.GONE
    }

    /**
     * Function that shows the request error. Shows a button to retry in case of connection/timeout error.
     *
     * @param message message to be shown to the user.
     * @param retry retry or not.
     */
    override fun showError(message: String, retry : Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getAuthoredChallenges(username)
            }
        }
        snackbar.show()
    }

    /**
     * Function that is invoked when a challenge is clicked.
     *
     * @param challengeID id of the challenge.
     */
    override fun onItemClicked(challengeID: String) {
        presenter.startChallengeDetailsActivity(challengeID)
    }

    /**
     * Function that starts the Challenge Details Activity with the challenges chosen.
     *
     * @param challengeID chosen challenge id.
     */
    override fun startChallengeDetailsActivity(challengeID: String) {
        val intent = Intent(activity, ChallengeDetailsActivity::class.java)
        intent.putExtra("challengeID", challengeID)
        startActivity(intent)
    }
}