package carlos.martins.codewars.challenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import carlos.martins.codewars.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_challenges.*

/**
 * Activity that shows the users challegens.
 *
 * @property authoredChallengesFragment Authored Challenges Fragment.
 * @property completedChallengesFragment Completed Challenges Fragment.
 */
class ChallengesActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    /**
     * Authored Challenges Fragment.
     */
    private lateinit var authoredChallengesFragment: AuthoredChallengesFragment

    /**
     * Completed Challenges Fragment.
     */
    private lateinit var completedChallengesFragment: CompletedChallengesFragment

    /**
     * Function that creates the activity.
     *
     * @param savedInstanceState the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenges)

        val username = intent.getStringExtra("username")

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "$username's Challenges"
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener{
            finish()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bottom_navigation.setOnNavigationItemSelectedListener(this)

        completedChallengesFragment = CompletedChallengesFragment.newInstance(username)
        authoredChallengesFragment = AuthoredChallengesFragment.newInstance(username)
        supportFragmentManager.beginTransaction().add(R.id.main_container, authoredChallengesFragment, "2").hide(authoredChallengesFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.main_container, completedChallengesFragment, "1").commit()
    }

    /**
     * Function invoked when is selected an item in the bottom navigation.
     *
     * @param item selected item.
     * @return true when an item is selected.
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_completed_challenges -> {
                supportFragmentManager.beginTransaction().hide(authoredChallengesFragment).show(completedChallengesFragment).commit()
                return true
            }
            R.id.action_authored_challenges -> {
                supportFragmentManager.beginTransaction().hide(completedChallengesFragment).show(authoredChallengesFragment).commit()
                return true
            }
        }
        return false
    }
}

