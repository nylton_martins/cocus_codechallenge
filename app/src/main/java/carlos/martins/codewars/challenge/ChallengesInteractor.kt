package carlos.martins.codewars.challenge

import carlos.martins.codewars.challenge.model.AuthoredChallengeResponse
import carlos.martins.codewars.challenge.model.CompletedChallengeResponse

/**
 * Challenges Interactor
 */
interface ChallengesInteractor {
    /**
     * Function that gets the user completed challenges.
     *
     * @param idOrUsername user's id/username.
     * @param page request page.
     * @param listener listener of the request response.
     */
    fun getCompletedChallenges(idOrUsername : String, page : Int, listener: ChallengesPresenterListener)

    /**
     * Function that gets the user authored challenges.
     *
     * @param idOrUsername user's id/username.
     * @param listener listener of the request response.
     */
    fun getAuthoredChallenges(idOrUsername : String, listener: ChallengesPresenterListener)

    /**
     * Challenges service response listener
     */
    interface ChallengesPresenterListener {
        /**
         * Function that is invoked when the user completed challenges get is successful.
         *
         * @param completedChallengeResponse Completed Challenge Response
         */
        fun onGetCompletedChallengesSuccess(completedChallengeResponse: CompletedChallengeResponse)

        /**
         * Function that is invoked when the user authored challenges get is successful.
         *
         * @param authoredChallengeResponse Authored Challenge Response
         */
        fun onGetAuthoredChallengesSuccess(authoredChallengeResponse: AuthoredChallengeResponse)

        /**
         * Function that is invoked when the user's challenges are not found.
         */
        fun onGetChallengesNotFound()

        /**
         * Function that is invoked when occurs a connection error.
         */
        fun onConnectionError()
    }
}