package carlos.martins.codewars.challenge

import carlos.martins.codewars.challenge.model.AuthoredChallengeResponse
import carlos.martins.codewars.challenge.model.CompletedChallengeResponse
import carlos.martins.codewars.challenge.service.ChallengesService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Implementation of the Challenges interactor.
 *
 * @property challengesService Challenges Service
 * @property disposable Disposable.
 * @constructor builds the interactor with the challenges service.
 */
class ChallengesInteractorImpl(var challengesService: ChallengesService) : ChallengesInteractor {

    /**
     * Disposable.
     */
    private var disposable: Disposable? = null

    /**
     * Function that gets the user completed challenges.
     *
     * @param idOrUsername user's id/username.
     * @param page request page.
     * @param listener listener of the request response.
     */
    override fun getCompletedChallenges(idOrUsername: String, page: Int, listener: ChallengesInteractor.ChallengesPresenterListener) {
        disposable = challengesService.getCompletedChallenges(idOrUsername, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleCompletedChallengesResult(result, listener)},
                {error -> handleRequestError(error.message, listener)}
            )
    }

    /**
     * Function that gets the user authored challenges.
     *
     * @param idOrUsername user's id/username.
     * @param listener listener of the request response.
     */
    override fun getAuthoredChallenges(idOrUsername: String, listener: ChallengesInteractor.ChallengesPresenterListener) {
        disposable = challengesService.getAuthoredChallenges(idOrUsername)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleAuthoredChallengesResult(result, listener)},
                {error -> handleRequestError(error.message, listener)}
            )
    }

    /**
     * Handles the get completed challenges result.
     *
     * @param completedChallengeResponse completed challenge response.
     * @param listener listener of the request response.
     */
    private fun handleCompletedChallengesResult(completedChallengeResponse: CompletedChallengeResponse, listener: ChallengesInteractor.ChallengesPresenterListener) {
        listener.onGetCompletedChallengesSuccess(completedChallengeResponse)
    }

    /**
     * Handles the get challenges response error.
     *
     * @param message error message.
     * @param listener listener of the request response.
     */
    private fun handleRequestError(message: String?, listener: ChallengesInteractor.ChallengesPresenterListener) {
        if(message != null && message.contains("404")) {
            listener.onGetChallengesNotFound()
        } else {
            listener.onConnectionError()
        }
    }

    /**
     * Handles the get authored challenges result.
     *
     * @param authoredChallengeResponse authored challenge response.
     * @param listener listener of the request response.
     */
    private fun handleAuthoredChallengesResult(authoredChallengeResponse: AuthoredChallengeResponse, listener: ChallengesInteractor.ChallengesPresenterListener) {
        listener.onGetAuthoredChallengesSuccess(authoredChallengeResponse)
    }
}