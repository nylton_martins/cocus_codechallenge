package carlos.martins.codewars.challenge

/**
 * Communicates between the model and view
 */
interface ChallengesPresenter {

    /**
     * Function that gets the user completed challenges.
     *
     * @param idOrUsername user's id/username.
     * @param page request page.
     */
    fun getCompletedChallenges(idOrUsername : String, page : Int)

    /**
     * Function that gets the user authored challenges.
     *
     * @param idOrUsername user's id/username.
     */
    fun getAuthoredChallenges(idOrUsername : String)

    /**
     * Function that starts the Challenge Details Activity with the challenges chosen.
     *
     * @param challengeID chosen challenge id.
     */
    fun startChallengeDetailsActivity(challengeID : String)
}