package carlos.martins.codewars.challenge

import carlos.martins.codewars.challenge.model.AuthoredChallengeResponse
import carlos.martins.codewars.challenge.model.CompletedChallengeResponse

/**
 * Implementation of the Challenges presenter
 *
 * @property view Challenges View
 * @property challengesInteractor Challenges Interactor
 * @constructor  builds a new Challenges Presenter.
 */
class ChallengesPresenterImpl(var view : ChallengesView?, var challengesInteractor : ChallengesInteractor) : ChallengesPresenter, ChallengesInteractor.ChallengesPresenterListener {

    /**
     * Function that gets the user completed challenges.
     *
     * @param idOrUsername user's id/username.
     * @param page request page.
     */
    override fun getCompletedChallenges(idOrUsername : String, page : Int) {
        if(page == 0) {
            view?.showLoading()
        }
        challengesInteractor.getCompletedChallenges(idOrUsername, page, this)
    }

    /**
     * Function that gets the user authored challenges.
     *
     * @param idOrUsername user's id/username.
     */
    override fun getAuthoredChallenges(idOrUsername : String) {
        view?.showLoading()
        challengesInteractor.getAuthoredChallenges(idOrUsername, this)
    }

    /**
     * Function that is invoked when the user authored challenges get is successful.
     *
     * @param authoredChallengeResponse Authored Challenge Response
     */
    override fun onGetAuthoredChallengesSuccess(authoredChallengeResponse: AuthoredChallengeResponse) {
        view?.hideLoading()
        view?.showAuthoredChallenges(authoredChallengeResponse.listOfAuthoredChallenges)
    }

    /**
     * Function that is invoked when the user completed challenges get is successful.
     *
     * @param completedChallengeResponse Completed Challenge Response
     */
    override fun onGetCompletedChallengesSuccess(completedChallengeResponse: CompletedChallengeResponse) {
        view?.hideLoading()
        view?.showCompletedChallenges(completedChallengeResponse.listOfCompletedChallenges, completedChallengeResponse.totalPages)
    }

    /**
     * Function that is invoked when the user is not found.
     */
    override fun onGetChallengesNotFound() {
        view?.hideLoading()
        view?.showError("Challenges Not Found.", false)
    }

    /**
     * Function that is invoked when occurs a connection error.
     */
    override fun onConnectionError() {
        view?.hideLoading()
        view?.showError("Connection Error.", true)
    }

    /**
     * Function that starts the Challenge Details Activity with the challenges chosen.
     *
     * @param challengeID chosen challenge id.
     */
    override fun startChallengeDetailsActivity(challengeID: String) {
        view?.startChallengeDetailsActivity(challengeID)
    }
}