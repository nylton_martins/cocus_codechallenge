package carlos.martins.codewars.challenge

import carlos.martins.codewars.challenge.model.AuthoredChallenge
import carlos.martins.codewars.challenge.model.CompletedChallenge

/**
 * Manages the view of the Challenges Activity
 */
interface ChallengesView {

    /**
     * Function that shows the completed challenges.
     *
     * @param completedChallenges list of completed challenges.
     */
    fun showCompletedChallenges(completedChallenges: List<CompletedChallenge>, totalPages : Int) {
    }

    /**
     * Function that shows the authored challenges.
     *
     * @param authoredChallenges list of authored challenges.
     */
    fun showAuthoredChallenges(authoredChallenges: List<AuthoredChallenge>) {
    }

    /**
     * Shows loading progress.
     */
    fun showLoading()

    /**
    * Hides loading progress.
    */
    fun hideLoading()

    /**
     * Function that shows the request error. Shows a button to retry in case of connection/timeout error.
     *
     * @param message message to be shown to the user.
     * @param retry retry or not.
     */
    fun showError(message : String, retry : Boolean)

    /**
     * Function that starts the Challenge Details Activity with the challenges chosen.
     *
     * @param challengeID chosen challenge id.
     */
    fun startChallengeDetailsActivity(challengeID: String)
}
