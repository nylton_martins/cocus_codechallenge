package carlos.martins.codewars.challenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import carlos.martins.codewars.R
import carlos.martins.codewars.challenge.model.AuthoredChallenge
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.authored_challenge_list_item.view.*

/**
 * Adapter for user's authored challenges.
 *
 * @property items list of users.
 * @property context context where it was invoked.
 * @constructor builds the adapter with the list and the context where it was invoked.
 */
class AuthoredChallengesListAdapter(private val items : ArrayList<AuthoredChallenge>,
                                    private val onChallengeClickListener: OnChallengeClickListener,
                                    private val context: Context) : RecyclerView.Adapter<AuthoredChallengesListAdapter.ViewHolder>() {

    /**
     * Function that creates the ViewHolder and inflates the layout
     *
     * @param parent ViewGroup view parent
     * @param viewType type of view
     * @return the new ViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.authored_challenge_list_item, parent, false))
    }

    /**
     * Function that changes the View according to the item in that position
     *
     * @param holder ViewHolder
     * @param position position of the item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = items[position]?.name
        items[position]?.tags.forEach {
            val chip = Chip(holder.chipGroup.context)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            holder.chipGroup.addView(chip)
        }
        holder.itemView.setOnClickListener {
            onChallengeClickListener.onItemClicked(items[position].id)
        }
    }

    /**
     * Function that returns the size of the projects list
     *
     * @return the size of the projects list
     */
    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * ViewHolder class that contains each view inside the item view
     *
     * @property name challenged name.
     * @property chipGroup challenge chip group. (Contains the tags)
     * @constructor builds with the view of the items
     */
    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val name: AppCompatTextView = view.name
        val chipGroup: ChipGroup = view.chips
    }
}

