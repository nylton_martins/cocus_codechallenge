package carlos.martins.codewars.challenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import carlos.martins.codewars.R
import carlos.martins.codewars.challenge.model.CompletedChallenge
import kotlinx.android.synthetic.main.completed_challenge_list_item.view.*
import kotlinx.android.synthetic.main.recycler_progress_item.view.*

/**
 * Adapter for user's completed challenges.
 *
 * @property items list of completed challenges.
 * @property context context where it was invoked.
 * @property onLoadMoreListener listener to be invoked when is needed to load more challenges.
 * @property VIEW_TYPE_ITEM View Type = 0 when view is of the item type.
 * @property VIEW_TYPE_LOADING View Type = 1 when view is of the progress type.
 * @constructor builds the adapter with the list and the context where it was invoked.
 */
class CompletedChallengesListAdapter(private val items : ArrayList<CompletedChallenge?>,
                                     private val context: Context,
                                     private val onChallengeClickListener: OnChallengeClickListener,
                                     private val onLoadMoreListener: LoadMoreListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * View Type = 0 when view is of the item type.
     */
    private val VIEW_TYPE_ITEM = 0
    /**
     * View Type = 1 when view is of the progress type.
     */
    private val VIEW_TYPE_LOADING = 1

    /**
     * Function that creates the ViewHolder and inflates the layout
     *
     * @param parent ViewGroup view parent
     * @param viewType type of view
     * @return the new ViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == VIEW_TYPE_ITEM) {
            ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.completed_challenge_list_item, parent, false))
        } else {
            ProgressViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_progress_item, parent, false))
        }
    }

    /**
     * Function that changes the View according to the item in that position
     *
     * @param holder ViewHolder
     * @param position position of the item
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ItemViewHolder) {
            holder.name.text = items[position]?.name
            holder.completedAt.text = items[position]?.completedAt
            holder.itemView.setOnClickListener {
                onChallengeClickListener.onItemClicked(items[position]?.id!!)
            }
        } else if(holder is ProgressViewHolder) {
            holder.progressBar.isIndeterminate = true
            onLoadMoreListener.onLoadMore()
        }
    }

    /**
     * Function that returns the size of the projects list
     *
     * @return the size of the projects list
     */
    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * Function that returns the view type. Returns loading when is the last position (null).
     *
     * @param position position of the view.
     * @return view type.
     */
    override fun getItemViewType(position: Int): Int {
        return if (items[position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    /**
     * ItemViewHolder class that contains each view inside the item view (Normal item)
     *
     * @property name name of the challenge.
     * @property completedAt date where the challenge was completed.
     * @constructor builds with the view of the items
     */
    class ItemViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val name: AppCompatTextView = view.name
        val completedAt: AppCompatTextView = view.completedAt
    }

    /**
     * ProgressViewHolder class that contains each view inside the item view (Progress item)
     *
     * @property progressBar progress bar
     * @constructor builds with the view of the items
     */
    class ProgressViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val progressBar: ProgressBar = view.progressBar
    }
}

