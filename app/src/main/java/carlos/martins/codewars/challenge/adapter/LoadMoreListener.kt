package carlos.martins.codewars.challenge.adapter

/**
 * Load more listener.
 */
interface LoadMoreListener {
    /**
     * Function that is invoked when is needed to load more items.
     */
    fun onLoadMore()
}