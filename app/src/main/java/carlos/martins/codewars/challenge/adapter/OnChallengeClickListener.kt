package carlos.martins.codewars.challenge.adapter

/**
 * On item click listener.
 */
interface OnChallengeClickListener {
    /**
     * Function that is invoked when an item is clicked.
     */
    fun onItemClicked(challengeID : String)
}