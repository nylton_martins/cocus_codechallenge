package carlos.martins.codewars.challenge.model

/**
 * Authored Challenge model.
 *
 * @property description challenge's description
 * @property id challenge's id
 * @property languages challenge's languages
 * @property name challenge's name
 * @property rank challenge's rank
 * @property rankName challenge's rank name
 * @property tags challenge's tags
 * @constructor builds a new Authored Challenge with the given properties
 */
data class AuthoredChallenge(
    val description: String,
    val id: String,
    val languages: List<String>,
    val name: String,
    val rank: Int,
    val rankName: String,
    val tags: List<String>
)