package carlos.martins.codewars.challenge.model

import com.google.gson.annotations.SerializedName

/**
 * Authored Challenge Response model.
 *
 * @property listOfAuthoredChallenges list of Authored Challenges.
 * @constructor builds a new Authored Challenge Response with the given properties
 */
data class AuthoredChallengeResponse(
    @SerializedName("data")
    val listOfAuthoredChallenges : List<AuthoredChallenge>
)