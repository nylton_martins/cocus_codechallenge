package carlos.martins.codewars.challenge.model

/**
 * Completed Challenge model.
 *
 * @property completedAt challenge's date pf completion
 * @property completedLanguages challenge's completed languages
 * @property id challenge's id
 * @property name challenge's name
 * @property slug challenge's slug
 * @constructor builds a new Completed Challenge with the given properties
 */
data class CompletedChallenge(
    val completedAt: String,
    val completedLanguages: List<String>,
    val id: String,
    val name: String,
    val slug: String
)