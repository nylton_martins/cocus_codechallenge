package carlos.martins.codewars.challenge.model

import com.google.gson.annotations.SerializedName

/**
 * Completed Challenge Response model.
 *
 * @property totalItems response total items.
 * @property totalPages response total pages.
 * @property listOfCompletedChallenges list of Completed Challenges.
 * @constructor builds a new Completed Challenge Response with the given properties
 */
data class CompletedChallengeResponse(
    val totalItems: Int,
    val totalPages: Int,
    @SerializedName("data")
    val listOfCompletedChallenges: List<CompletedChallenge>
)