package carlos.martins.codewars.challenge.service

import carlos.martins.codewars.challenge.model.AuthoredChallengeResponse
import carlos.martins.codewars.challenge.model.CompletedChallengeResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/**
 * Challenges API Service
 */
interface ChallengesService {

    /**
     * Function that gets the completed challenges of a user
     *
     * @param idOrUsername id/username of the user
     * @param page page of the response
     * @return Observable<CompletedChallengeResponse>
     */
    @GET("{idOrUsername}/code-challenges/completed")
    fun getCompletedChallenges(@Path("idOrUsername") idOrUsername: String, @Query("page") page: Int):
            Observable<CompletedChallengeResponse>

    /**
     * Function that gets the authored challenges of a user
     *
     * @param idOrUsername id/username of the user
     * @return Observable<AuthoredChallengeResponse>
     */
    @GET("{idOrUsername}/code-challenges/authored")
    fun getAuthoredChallenges(@Path("idOrUsername") idOrUsername: String):
            Observable<AuthoredChallengeResponse>

    companion object {
        /**
         * Function that creates a new instance of the Challenges API Service
         *
         * @return the new instance of the Service
         */
        fun create(): ChallengesService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("https://www.codewars.com/api/v1/users/")
                .build()

            return retrofit.create(ChallengesService::class.java)
        }
    }
}