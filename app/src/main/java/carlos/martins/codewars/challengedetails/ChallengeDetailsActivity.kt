package carlos.martins.codewars.challengedetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.widget.Toolbar
import carlos.martins.codewars.R
import carlos.martins.codewars.challengedetails.model.ChallengeDetails
import carlos.martins.codewars.challengedetails.service.ChallengeDetailsService
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_challenge_details.*

/**
 * Activity to show the challenge details.
 *
 * @property presenter presenter to communicate between the model and view.
 * @property challengeID challenge ir or slug.
 */
class ChallengeDetailsActivity : AppCompatActivity(), ChallengeDetailsView {

    /**
     * Search presenter to communicate between the model and view.
     */
    private val presenter : ChallengeDetailsPresenter = ChallengeDetailsPresenterImpl(this, ChallengeDetailsInteractorImpl(
        ChallengeDetailsService.create()))

    /**
     * Challenge ir or slug.
     */
    private lateinit var challengeID : String

    /**
     * Function that creates the activity.
     *
     * @param savedInstanceState the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenge_details)

        challengeID = intent.getStringExtra("challengeID")

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Challenge Details"
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener{
            finish()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.getChallengeDetails(challengeID)
    }

    /**
     * Shows the challenge details.
     *
     * @param challengeDetails Challenge Details
     */
    override fun showChallengeDetails(challengeDetails: ChallengeDetails) {
        challengeDetails.tags.forEach {
            val chip = Chip(this)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            tagChips.addView(chip)
        }
        challengeDetails.languages.forEach {
            val chip = Chip(this)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            languageChips.addView(chip)
        }
        description.text = Html.fromHtml(challengeDetails.description)
        name.text = challengeDetails.name
        category.text = challengeDetails.category
    }

    /**
     * Function that hides keyboard and shows the loading progress.
     */
    override fun showLoading() {
        progressLayout.visibility = View.VISIBLE
        progressBar.isIndeterminate = true
    }

    /**
     * Function that hides the loading progress.
     */
    override fun hideLoading() {
        progressLayout.visibility = View.GONE
    }

    /**
     * Function that shows the request error. Shows a button to retry in case of connection/timeout error.
     *
     * @param message message to be shown to the user.
     * @param retry retry or not.
     */
    override fun showError(message: String, retry : Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getChallengeDetails(challengeID)
            }
        }
        snackbar.show()
    }
}
