package carlos.martins.codewars.challengedetails

import carlos.martins.codewars.challengedetails.model.ChallengeDetails

/**
 * Challenge Details Interactor
 */
interface ChallengeDetailsInteractor {
    /**
     * Function that gets the challenge details.
     *
     * @param irOrSlug id or slug of the challenge.
     */
    fun getChallengeDetails(irOrSlug : String, listener: ChallengeDetailsPresenterListener)

    /**
     * Challenge Details service response listener
     */
    interface ChallengeDetailsPresenterListener {

        /**
         * Function that is invoked when the user get is successful.
         *
         * @param challengeDetails challenge details.
         */
        fun onGetChallengeByIDSuccess(challengeDetails: ChallengeDetails)

        /**
         * Function that is invoked when the challenge details are not found.
         */
        fun onChallengeDetailsNotFound()

        /**
         * Function that is invoked when occurs a connection error.
         */
        fun onConnectionError()
    }
}