package carlos.martins.codewars.challengedetails

import carlos.martins.codewars.challengedetails.model.ChallengeDetails
import carlos.martins.codewars.challengedetails.service.ChallengeDetailsService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Implementation of the Challenge Details interactor.
 *
 * @property challengeDetailsService Challenge Details Service
 * @property disposable disposable.
 * @constructor builds the interactor with the challenge details service.
 */
class ChallengeDetailsInteractorImpl(var challengeDetailsService : ChallengeDetailsService) : ChallengeDetailsInteractor {

    /**
     * Disposable.
     */
    private var disposable: Disposable? = null

    /**
     * Function that gets the challenge details.
     *
     * @param irOrSlug id or slug of the challenge.
     */
    override fun getChallengeDetails(irOrSlug: String, listener: ChallengeDetailsInteractor.ChallengeDetailsPresenterListener) {
        disposable = challengeDetailsService.getChallengeDetails(irOrSlug)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleRequestResult(result, listener)},
                {error -> handleRequestError(error.message, listener)}
            )
    }

    /**
     * Handles the get challenge details result.
     *
     * @param challengeDetails challenge details given in response.
     * @param listener listener of the request response.
     */
    private fun handleRequestResult(challengeDetails: ChallengeDetails, listener: ChallengeDetailsInteractor.ChallengeDetailsPresenterListener) {
        listener.onGetChallengeByIDSuccess(challengeDetails)
    }

    /**
     * Handles the search response error.
     *
     * @param message error message.
     * @param listener listener of the request response.
     */
    private fun handleRequestError(message: String?, listener: ChallengeDetailsInteractor.ChallengeDetailsPresenterListener) {
        if(message != null && message.contains("404")) {
            listener.onChallengeDetailsNotFound()
        } else {
            listener.onConnectionError()
        }
    }
}