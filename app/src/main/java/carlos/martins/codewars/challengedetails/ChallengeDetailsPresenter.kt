package carlos.martins.codewars.challengedetails

/**
 * Communicates between the model and view
 */
interface ChallengeDetailsPresenter {

    /**
     * Function that gets the challenge details.
     *
     * @param irOrSlug id or slug of the challenge.
     */
    fun getChallengeDetails(irOrSlug : String)
}