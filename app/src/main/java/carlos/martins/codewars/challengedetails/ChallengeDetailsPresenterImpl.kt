package carlos.martins.codewars.challengedetails

import carlos.martins.codewars.challengedetails.model.ChallengeDetails

/**
 * Implementation of the Challenge Details presenter
 *
 * @property view Challenge Details View
 * @property challengesInteractor Challenge Details Interactor
 * @constructor builds a new Challenge Details Presenter.
 */
class ChallengeDetailsPresenterImpl(var view : ChallengeDetailsView?, var challengesInteractor : ChallengeDetailsInteractor) : ChallengeDetailsPresenter, ChallengeDetailsInteractor.ChallengeDetailsPresenterListener {
    /**
     * Function that gets the challenge details.
     *
     * @param irOrSlug id or slug of the challenge.
     */
    override fun getChallengeDetails(irOrSlug : String) {
        view?.showLoading()
        challengesInteractor.getChallengeDetails(irOrSlug, this)
    }

    /**
     * Function that is invoked when the user get is successful.
     *
     * @param challengeDetails challenge details.
     */
    override fun onGetChallengeByIDSuccess(challengeDetails: ChallengeDetails) {
        view?.hideLoading()
        view?.showChallengeDetails(challengeDetails)
    }

    /**
     * Function that is invoked when the challenge details are not found.
     */
    override fun onChallengeDetailsNotFound() {
        view?.hideLoading()
        view?.showError("Challenge Details Not Found", false)
    }

    /**
     * Function that is invoked when occurs a connection error.
     */
    override fun onConnectionError() {
        view?.hideLoading()
        view?.showError("Connection Error", true)
    }
}