package carlos.martins.codewars.challengedetails

import carlos.martins.codewars.challengedetails.model.ChallengeDetails

/**
 * Manages the view of the Challenge Details Activity
 */
interface ChallengeDetailsView {

    /**
     * Shows the challenge details.
     *
     * @param challengeDetails Challenge Details
     */
    fun showChallengeDetails(challengeDetails: ChallengeDetails)

    /**
     * Function that shows the loading progress.
     */
    fun showLoading()

    /**
     * Function that hides the loading progress.
     */
    fun hideLoading()

    /**
     * Function that shows the request error. Shows a button to retry in case of connection/timeout error.
     *
     * @param message message to be shown to the user.
     * @param retry retry or not.
     */
    fun showError(message : String, retry : Boolean)
}