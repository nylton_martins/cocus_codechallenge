package carlos.martins.codewars.challengedetails.model

/**
 * Challenge Details Model
 *
 * @property approvedAt challenge date of approval
 * @property approvedBy challenge approved by
 * @property category challenge category
 * @property createdBy challenge created by
 * @property description challenge description
 * @property id challenge id
 * @property languages challenge languages
 * @property name challenge name
 * @property publishedAt challenge date of publish
 * @property rank challenge rank
 * @property slug challenge slug
 * @property tags challenge tags
 * @property totalAttempts challenge total attempts
 * @property totalCompleted challenge total completed
 * @property totalStars challenge total stars
 * @property url challenge url
 * @constructor builds a new challenge with the given properties
 */
data class ChallengeDetails(
    val approvedAt: String,
    val approvedBy: ApprovedBy,
    val category: String,
    val createdBy: CreatedBy,
    val description: String,
    val id: String,
    val languages: List<String>,
    val name: String,
    val publishedAt: String,
    val rank: Rank,
    val slug: String,
    val tags: List<String>,
    val totalAttempts: Int,
    val totalCompleted: Int,
    val totalStars: Int,
    val url: String
)

/**
 * Rank model
 *
 * @property color rank color
 * @property id rank id
 * @property name rank name
 * @constructor builds a new Rank with the given properties
 */
data class Rank(
    val color: String,
    val id: Int,
    val name: String
)

/**
 * Created By Model
 *
 * @property url created by url
 * @property username created by username
 * @constructor builds a new CreatedBy model with the given properties
 */
data class CreatedBy(
    val url: String,
    val username: String
)

/**
 * Approved By Model
 *
 * @property url approved by url
 * @property username approved by username
 * @constructor builds a new ApprovedBy model with the given properties
 */
data class ApprovedBy(
    val url: String,
    val username: String
)