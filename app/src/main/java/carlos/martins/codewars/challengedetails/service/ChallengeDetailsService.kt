package carlos.martins.codewars.challengedetails.service

import carlos.martins.codewars.challengedetails.model.ChallengeDetails
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/**
 * Challenges Details API Service
 */
interface ChallengeDetailsService {

    /**
     * Function that gets the details of a challenge.
     *
     * @param idOrSlug id or slug of the challenge
     * @return Observable<ChallengeDetails>
     */
    @GET("{idOrSlug}")
    fun getChallengeDetails(@Path("idOrSlug") idOrSlug: String):
            Observable<ChallengeDetails>

    companion object {
        /**
         * Function that creates a new instance of the Challenges Details API Service
         *
         * @return the new instance of the Service
         */
        fun create(): ChallengeDetailsService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("https://www.codewars.com/api/v1/code-challenges/")
                .build()

            return retrofit.create(ChallengeDetailsService::class.java)
        }
    }
}