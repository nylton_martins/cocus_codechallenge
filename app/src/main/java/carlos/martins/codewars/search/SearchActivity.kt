package carlos.martins.codewars.search

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import carlos.martins.codewars.R
import carlos.martins.codewars.challenge.ChallengesActivity
import carlos.martins.codewars.search.adapter.UserListAdapter
import carlos.martins.codewars.search.model.User
import carlos.martins.codewars.search.service.CodewarsService
import carlos.martins.codewars.utils.Utils
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_search.*

/**
 * Activity to manage the user search.
 *
 * @property presenter presenter to communicate between the model and view.
 * @property userList list of users.
 */
class SearchActivity : AppCompatActivity(), SearchView, View.OnClickListener {

    /**
     * Search presenter to communicate between the model and view.
     */
    private val presenter : SearchPresenter = SearchPresenterImpl(this, SearchInteractorImpl(CodewarsService.create()))
    /**
     * List of users.
     */
    private var userList = emptyList<User>()

    /**
     * Function that creates the activity.
     *
     * @param savedInstanceState the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Codewars"
        setSupportActionBar(toolbar)

        presenter.getRoomDatabase()
        presenter.observeUsers()

        val layoutManager = LinearLayoutManager(this)
        usersListRecycler.layoutManager = layoutManager

        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayOf("History(Time)", "Best Rank"))
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sortBySpinner.adapter = arrayAdapter
        sortBySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(sortBySpinner.adapter.getItem(position)) {
                    "History(Time)" -> {
                        sortUserList(userList)
                    }
                    "Best Rank" -> {
                        sortUserList(userList)
                    }
                }
            }
        }
    }

    /**
     * Function that handles the click on the activity views.
     *
     * @param v clicked view
     */
    override fun onClick(v: View?) {
        when(v) {
            searchButton -> {
                if(searchText.text.toString() == "") {
                    showError("Query should not be empty.", false)
                } else {
                    presenter.getUserByIDOrUsername(searchText.text.toString())
                }
            }
        }
    }

    /**
     * Function that shows the list of users in the Recycler View.
     *
     * @param usersList users list
     */
    override fun showUsers(usersList: List<User>) {
        sortUserList(usersList)
    }

    /**
     * Function that starts the Challenges Activity with the challenges for the chosen user.
     *
     * @param user chosen user
     */
    override fun startChallengesActivity(user: User) {
        val intent = Intent(this, ChallengesActivity::class.java)
        intent.putExtra("username", user.username)
        startActivity(intent)
    }

    /**
     * Function that sorts the user list according to the selected way.
     * Also replaces the recycler view adapter with the new list.
     *
     * @param usersList list to be sorted.
     */
    private fun sortUserList(usersList: List<User>){
        userList = if(sortBySpinner.selectedItem == "History(Time)") {
            Utils.sortUserListByTimeOrRank(usersList, true)
        } else {
            Utils.sortUserListByTimeOrRank(usersList, false)
        }
        val adapter = UserListAdapter(ArrayList(userList), this)
        usersListRecycler.adapter = adapter
    }

    /**
     * Function that hides keyboard and shows the loading progress.
     */
    override fun showLoading() {
        hideKeyboard()
        progressLayout.visibility = View.VISIBLE
        progressBar.isIndeterminate = true
    }

    /**
     * Function that hides the loading progress.
     */
    override fun hideLoading() {
        progressLayout.visibility = View.GONE
    }

    /**
     * Function that shows the request error. Shows a button to retry in case of connection/timeout error.
     *
     * @param message message to be shown to the user.
     * @param retry retry or not.
     */
    override fun showError(message: String, retry : Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getUserByIDOrUsername(searchText.text.toString())
            }
        }
        snackbar.show()
    }

    /**
     * Function that hides the keyboard.
     */
    private fun hideKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(rootView.windowToken, 0)
    }
}
