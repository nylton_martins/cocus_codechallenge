package carlos.martins.codewars.search

import android.content.Context
import androidx.lifecycle.LiveData
import carlos.martins.codewars.search.model.User

/**
 * Search Interactor
 */
interface SearchInteractor {
    /**
     * Function that gets the room database instance.
     *
     * @param context context where it was invoked.
     */
    fun getRoomDatabase(context : Context)

    /**
     * Function that gets a user by its id or username.
     *
     * @param idOrUsername user's id/username
     * @param listener listener of the request response.
     */
    fun getUserByIDOrUsername(idOrUsername : String, listener: SearchPresenterListener)

    /**
     * Function that inserts an user to the db.
     *
     * @param user user to be inserted.
     * @param listener listener of the request response
     */
    fun saveUserToDatabase(user : User, listener: SearchPresenterListener)

    /**
     * Function that inserts an user to the db and deletes the oldest one.
     *
     * @param oldUser oldest user to be deleted.
     * @param user user to be inserted.
     * @param listener listener of the request response
     */
    fun substituteUserOnDatabase(oldUser : User, user : User, listener: SearchPresenterListener)

    /**
     * Function that gets all users.
     *
     * @return a LiveData of the list of users
     */
    fun getAllUsers() : LiveData<List<User>>

    /**
     * Codewars service response listener
     */
    interface SearchPresenterListener {

        /**
         * Function that is invoked when the user get is successful.
         *
         * @param user user.
         */
        fun onGetUserByIDSuccess(user: User)

        /**
         * Function that is invoked when the user is inserted in the db with success.
         *
         * @param user user.
         */
        fun onUserInsertedWithSuccess(user: User)

        /**
         * Function that is invoked when the user is not found.
         */
        fun onUserNotFound()

        /**
         * Function that is invoked when occurs a connection error.
         */
        fun onConnectionError()
    }
}