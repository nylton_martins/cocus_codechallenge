package carlos.martins.codewars.search

import android.content.Context
import androidx.lifecycle.LiveData
import carlos.martins.codewars.search.model.User
import carlos.martins.codewars.search.persistence.UserDao
import carlos.martins.codewars.search.service.CodewarsService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import carlos.martins.codewars.search.persistence.UserDatabase

/**
 * Implementation of the Search interactor.
 *
 * @property codewarsService Codewars Service.
 * @property userDao User DAO (Data Access Object).
 * @property allUsers live data of the list of users.
 * @property disposable disposable.
 * @constructor builds the interactor with the codewars service.
 */
class SearchInteractorImpl(var codewarsService : CodewarsService) : SearchInteractor {

    /**
     * User DAO (Data Access Object).
     */
    private lateinit var userDao: UserDao
    /**
     * Live data of the list of users.
     */
    private lateinit var allUsers: LiveData<List<User>>

    /**
     * Disposable.
     */
    private var disposable: Disposable? = null

    /**
     * Function that gets the room database instance.
     *
     * @param context context where it was invoked.
     */
    override fun getRoomDatabase(context: Context) {
        val userDatabase = UserDatabase.getInstance(context)
        userDao = userDatabase.userDao()
        allUsers = userDao.getAllUsers()
    }

    /**
     * Function that gets a user by its id or username.
     *
     * @param idOrUsername user's id/username
     * @param listener listener of the request response.
     */
    override fun getUserByIDOrUsername(idOrUsername: String, listener: SearchInteractor.SearchPresenterListener) {
        disposable = codewarsService.getUserByIDOrUsername(idOrUsername)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleSearchResult(result, listener)},
                {error -> handleSearchError(error.message, listener)}
            )
    }

    /**
     * Handles the search result.
     *
     * @param user user given in response.
     * @param listener listener of the request response.
     */
    private fun handleSearchResult(user: User, listener: SearchInteractor.SearchPresenterListener) {
        listener.onGetUserByIDSuccess(user)
    }

    /**
     * Handles the search response error.
     *
     * @param message error message.
     * @param listener listener of the request response.
     */
    private fun handleSearchError(message: String?, listener: SearchInteractor.SearchPresenterListener) {
        if(message != null && message.contains("404")) {
            listener.onUserNotFound()
        } else {
            listener.onConnectionError()
        }
    }

    /**
     * Function that inserts an user to the db.
     *
     * @param user user to be inserted.
     * @param listener listener of the request response
     */
    override fun saveUserToDatabase(user: User, listener: SearchInteractor.SearchPresenterListener) {
        disposable = userDao.insertUser(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {},
                {error -> handleSearchError(error.message, listener)}
            )
    }

    /**
     * Function that inserts an user to the db and deletes the oldest one.
     *
     * @param oldUser oldest user to be deleted.
     * @param user user to be inserted.
     * @param listener listener of the request response
     */
    override fun substituteUserOnDatabase(oldUser : User, user: User, listener: SearchInteractor.SearchPresenterListener) {
        disposable = userDao.deleteUser(oldUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {listener.onUserInsertedWithSuccess(user)},
                {error -> handleSearchError(error.message, listener)}
            )

    }

    /**
     * Function that gets all users.
     *
     * @return a LiveData of the list of users
     */
    override fun getAllUsers() : LiveData<List<User>> {
        return allUsers
    }
}