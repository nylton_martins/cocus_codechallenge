package carlos.martins.codewars.search

/**
 * Communicates between the model and view
 */
interface SearchPresenter {
    /**
     * Function that gets the room database instance.
     */
    fun getRoomDatabase()

    /**
     * Function that gets a user by its id or username.
     *
     * @param idOrUsername user's id/username
     */
    fun getUserByIDOrUsername(idOrUsername : String)

    /**
     * Function that creates an observer on the users list Live Data.
     */
    fun observeUsers()
}