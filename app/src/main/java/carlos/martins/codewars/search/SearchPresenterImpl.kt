package carlos.martins.codewars.search

import android.content.Context
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import carlos.martins.codewars.search.model.User
import carlos.martins.codewars.utils.Utils

/**
 * Implementation of the Search presenter
 *
 * @property view Search View
 * @property searchInteractor Search Interactor
 * @constructor builds a new Search Presenter.
 */
class SearchPresenterImpl (var view : SearchView?, var searchInteractor: SearchInteractor) : SearchPresenter, SearchInteractor.SearchPresenterListener {

    /**
     * Function that gets the room database instance.
     */
    override fun getRoomDatabase() {
        searchInteractor.getRoomDatabase(view as Context)
    }

    /**
     * Function that gets a user by its id or username.
     *
     * @param idOrUsername user's id/username
     */
    override fun getUserByIDOrUsername(idOrUsername: String) {
        view?.showLoading()
        searchInteractor.getUserByIDOrUsername(idOrUsername, this)
    }

    /**
     * Function that creates an observer on the users list Live Data.
     */
    override fun observeUsers() {
        view?.showLoading()
        searchInteractor.getAllUsers().observe(view as LifecycleOwner, Observer { users ->
            users?.let {
                view?.hideLoading()
                view?.showUsers(users)
            }
        })

    }

    /**
     * Function that is invoked when the user get is successful.
     *
     * @param user user.
     */
    override fun onGetUserByIDSuccess(user: User) {
        val listOfUsers = searchInteractor.getAllUsers().value
        if(listOfUsers != null) {
            if (listOfUsers.size < 5 || Utils.contains(listOfUsers, user)) {
                searchInteractor.saveUserToDatabase(user, this)
            } else {
                val oldestUser = Utils.getOldestUser(listOfUsers)
                if (oldestUser != null) {
                    searchInteractor.substituteUserOnDatabase(oldestUser, user, this)
                }
            }
        }
    }

    /**
     * Function that is invoked when the user is inserted in the db with success.
     *
     * @param user user.
     */
    override fun onUserInsertedWithSuccess(user : User) {
        searchInteractor.saveUserToDatabase(user, this)
    }

    /**
     * Function that is invoked when the user is not found.
     */
    override fun onUserNotFound() {
        view?.hideLoading()
        view?.showError("User Not Found", false)
    }

    /**
     * Function that is invoked when occurs a connection error.
     */
    override fun onConnectionError() {
        view?.hideLoading()
        view?.showError("Connection Error", true)
    }
}