package carlos.martins.codewars.search

import carlos.martins.codewars.search.model.User

/**
 * Manages the view of the Search Activity
 */
interface SearchView {
    /**
     * Function that shows the list of users in the Recycler View.
     *
     * @param usersList users list
     */
    fun showUsers(usersList : List<User>)

    /**
     * Function that starts the Challenges Activity with the challenges for the chosen user.
     *
     * @param user chosen user
     */
    fun startChallengesActivity(user: User)

    /**
     * Function that hides keyboard and shows the loading progress.
     */
    fun showLoading()

    /**
     * Function that hides the loading progress.
     */
    fun hideLoading()

    /**
     * Function that shows the request error. Shows a button to retry in case of connection/timeout error.
     *
     * @param message message to be shown to the user.
     * @param retry retry or not.
     */
    fun showError(message : String, retry : Boolean)
}