package carlos.martins.codewars.search.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import carlos.martins.codewars.R
import carlos.martins.codewars.search.SearchView
import carlos.martins.codewars.search.model.User
import kotlinx.android.synthetic.main.user_list_item.view.*

/**
 * Adapter for user list history.
 *
 * @property items list of users.
 * @property searchView search view.
 * @constructor builds the adapter with the list and the view where it was invoked.
 */
class UserListAdapter(private val items : ArrayList<User>, private val searchView: SearchView) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    /**
     * Function that creates the ViewHolder and inflates the layout
     *
     * @param parent ViewGroup view parent
     * @param viewType type of view
     * @return the new ViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(searchView as Context).inflate(R.layout.user_list_item, parent, false))
    }

    /**
     * Function that changes the View according to the item in that position
     *
     * @param holder ViewHolder
     * @param position position of the item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            searchView.startChallengesActivity(items[position])
        }
        holder.username.text = items[position].username
        holder.rank.text = "${items[position].leaderboardPosition}"
        holder.bestLanguage.text = "${items[position].getBestLanguage().languageName} (${items[position].getBestLanguage().score} points)"
    }

    /**
     * Function that returns the size of the projects list
     *
     * @return the size of the projects list
     */
    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * ViewHolder class that contains each view inside the item view
     *
     * @property username user's username
     * @property rank user's rank
     * @property bestLanguage user's best language
     * @constructor builds with the view of the items
     */
    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val username: AppCompatTextView = view.username
        val rank: AppCompatTextView = view.rank
        val bestLanguage: AppCompatTextView = view.bestLanguage
    }
}

