package carlos.martins.codewars.search.model

/**
 * Language model
 *
 * @property languageName language name
 * @property rank language rank
 * @property name language name
 * @property color language color
 * @property score language score
 * @constructor builds a new language with the given properties
 */
data class Language (
    var languageName : String?,
    var rank : Int,
    var name : String?,
    var color : String?,
    var score : Int
) {
    /**
     * Empty constructor
     *
     * @constructor builds an empty language
     */
    constructor() : this("",0,"","",0)
}