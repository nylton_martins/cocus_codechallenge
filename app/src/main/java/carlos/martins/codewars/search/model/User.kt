package carlos.martins.codewars.search.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * User model with mapping for DB and Request
 *
 * @property username user username
 * @property name user name
 * @property languages user languages
 * @property timestamp user search timestamp
 * @property leaderboardPosition user leaderboard position
 * @constructor builds a new user with the given properties
 */
@Entity(tableName = "users")
data class User(
    @PrimaryKey
    @ColumnInfo(name = "username")
    @SerializedName("username")
    var username : String,

    @ColumnInfo(name = "name")
    @SerializedName("name")
    var name : String?,

    @ColumnInfo(name = "languages")
    var languages : List<Language>?,

    @ColumnInfo(name = "searchedTime")
    var timestamp: Date,

    @ColumnInfo(name = "leaderboardPosition")
    @SerializedName("leaderboardPosition")
    var leaderboardPosition : Int?) {

    /**
     * Empty constructor
     *
     * @constructor builds an empty user
     */
    constructor() : this("","", emptyList(), Date(), 0)

    /**
     * Function that returns the user's best language
     *
     * @return user's best language
     */
    fun getBestLanguage() : Language {
        var language = Language()
        languages?.forEach {
            if(it.score > language.score) {
                language = it
            }
        }
        return language
    }
}