package carlos.martins.codewars.search.persistence

import androidx.room.TypeConverter
import carlos.martins.codewars.search.model.Language
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.util.*

/**
 * Convert non primitive types to be able to store in the DB
 */
class Converters {
    companion object {

        /**
         * Converts a list of languages to a string
         *
         * @param list list of languages
         * @return converted string
         */
        @TypeConverter
        @JvmStatic
        fun fromListOfLanguages(list: List<Language>): String {
            val gson = Gson()
            val json = gson.toJson(list)
            return json
        }

        /**
         * Converts a string with the list of languages
         *
         * @param listString converted string
         * @return list of languages
         */
        @TypeConverter
        @JvmStatic
        fun toListOfLanguages(listString: String):  List<Language> {
            val parser = JsonParser()
            var languagesArray = arrayListOf<Language>()
            val languagesObject = parser.parse(listString).asJsonArray
            languagesObject.forEach {
                var language = Language()
                it as JsonObject
                language.languageName = it.get("languageName")?.asString
                if(it.has("rank")) {
                    language.rank = it?.get("rank")?.asInt!!
                }
                language.name = it?.get("name")?.asString
                language.color = it?.get("color")?.asString
                if(it.has("score")) {
                    language.score = it?.get("score")?.asInt!!
                }

                languagesArray.add(language)
            }

            return languagesArray
        }

        /**
         * Converts a timestamp to a date
         *
         * @param value timestamp
         * @return date
         */
        @TypeConverter
        @JvmStatic
        fun fromTimestamp(value: Long): Date? {
            return if (value == null) null else Date(value)
        }

        /**
         * Converts a date to a timestamp
         *
         * @param date date to be converted
         * @return timestamp
         */
        @TypeConverter
        @JvmStatic
        fun dateToTimestamp(date: Date): Long? {
            return if (date == null) null else date!!.time
        }
    }
}