package carlos.martins.codewars.search.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import carlos.martins.codewars.search.model.User
import io.reactivex.Completable
import io.reactivex.Flowable
import java.nio.file.Files.delete
import androidx.room.Transaction
import java.util.*

/**
 * User DAO (Data Access Object).
 */
@Dao
interface UserDao {

    /**
     * Gets all users from the db.
     *
     * @return the list of the existing users.
     */
    @Query("SELECT * FROM Users")
    fun getAllUsers(): LiveData<List<User>>

    /**
     * Query that inserts the given user in the db.
     * If user already exists it's replaced.
     *
     * @param user user to be inserted in the table.
     * @return indicates the completion of the insert.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Completable

    /**
     * Query that gets the user in the db with the given id/username.
     *
     * @param idOrUsername id/username of the User.
     * @return a Flowable of User.
     */
    @Query("SELECT * FROM Users WHERE username = :idOrUsername")
    fun getUserByIdOrUsername(idOrUsername : String) : Flowable<User>

    /**
     * Query that deletes all users in the table.
     */
    @Query("DELETE FROM Users")
    fun deleteAllUsers()

    /**
     * Query that deletes a user in the table.
     */
    @Delete
    fun deleteUser(user: User) : Completable
}