package carlos.martins.codewars.search.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import carlos.martins.codewars.search.model.User

/**
 * Database that contains users.
 */
@Database(entities = arrayOf(User::class), version = 1)
@TypeConverters(Converters::class)
abstract class UserDatabase : RoomDatabase() {

    /**
     * User DAO (Data Access Object).
     *
     * @return user DAO
     */
    abstract fun userDao(): UserDao

    companion object {

        /**
         * Database instance
         */
        @Volatile private var INSTANCE: UserDatabase? = null

        /**
         * Function that returns the db instance
         *
         * @param context context where it was invoked
         * @return the db instance
         */
        fun getInstance(context: Context): UserDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        /**
         * Function that builds a new database
         *
         * @param context context where it was invoked
         * @return the db instance
         */
        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                UserDatabase::class.java, "CodewarsDB.db")
                .build()
    }
}