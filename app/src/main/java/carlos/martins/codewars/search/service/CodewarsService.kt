package carlos.martins.codewars.search.service

import carlos.martins.codewars.search.model.User
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * Codewars API Service
 */
interface CodewarsService {

    /**
     * Function that gets a user by it's id or username
     *
     * @param idOrUsername id/username of the user
     * @return Observable<User>
     */
    @GET("users/{idOrUsername}")
    fun getUserByIDOrUsername(@Path("idOrUsername") idOrUsername: String):
            Observable<User>

    companion object {
        /**
         * Function that creates a new instance of the Codewars API Service
         *
         * @return the new instance of the Service
         */
        fun create(): CodewarsService {
            val gsonBuilder = GsonBuilder()
            gsonBuilder.registerTypeAdapter(User::class.java, UserDeserializer())
            val gson = gsonBuilder.create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create(gson))
                .baseUrl("https://www.codewars.com/api/v1/")
                .build()

            return retrofit.create(CodewarsService::class.java)
        }
    }
}