package carlos.martins.codewars.utils

import carlos.martins.codewars.search.model.User
import java.util.*

/**
 * Class that contains general utility used in the application
 */
class Utils {
    companion object {

        /**
         * Function that returns the oldest user from the list.
         *
         * @param userList list of users.
         * @return oldest user.
         */
        fun getOldestUser(userList : List<User>) : User? {
            var auxDate = Date()
            var user = User()
            userList.forEach {
                if(it.timestamp < auxDate) {
                    auxDate = it.timestamp
                    user = it
                }
            }
            return user
        }

        /**
         * Function that sorts the users list by time or rank.
         *
         * @param usersList users list.
         * @param time true if sorting by time, false if sorting by rank.
         * @return sorted list.
         */
        fun sortUserListByTimeOrRank(usersList: List<User>, time: Boolean) : List<User> {
            return if(time) {
                usersList.sortedWith(compareByDescending { it.timestamp })
            } else {
                usersList.sortedWith(compareBy { it.leaderboardPosition })
            }
        }

        /**
         * Function that checks if the users list contains the given user.
         *
         * @param usersList users list.
         * @param user user to be searched.
         * @return true if contains, false if not.
         */
        fun contains(usersList: List<User>, user: User) : Boolean {
            usersList.forEach {
                if(it.username == user.username) {
                    return true
                }
            }
            return false
        }
    }
}