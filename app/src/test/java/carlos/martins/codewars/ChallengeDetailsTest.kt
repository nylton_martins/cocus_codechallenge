package carlos.martins.codewars

import android.accounts.NetworkErrorException
import carlos.martins.codewars.challengedetails.ChallengeDetailsInteractorImpl
import carlos.martins.codewars.challengedetails.ChallengeDetailsPresenterImpl
import carlos.martins.codewars.challengedetails.ChallengeDetailsView
import carlos.martins.codewars.challengedetails.model.ApprovedBy
import carlos.martins.codewars.challengedetails.model.ChallengeDetails
import carlos.martins.codewars.challengedetails.model.CreatedBy
import carlos.martins.codewars.challengedetails.model.Rank
import carlos.martins.codewars.challengedetails.service.ChallengeDetailsService
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import okhttp3.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import java.util.concurrent.Executor

@RunWith(JUnit4::class)
class ChallengeDetailsTest {

    private lateinit var presenter: ChallengeDetailsPresenterImpl
    private lateinit var interactor: ChallengeDetailsInteractorImpl

    @Mock
    private lateinit var challengeDetailsView: ChallengeDetailsView
    @Mock
    private lateinit var challengeDetailsService: ChallengeDetailsService

    @Before
    fun setup() {
        val immediate = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }
        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        MockitoAnnotations.initMocks(this)

        interactor = ChallengeDetailsInteractorImpl(challengeDetailsService)
        presenter = ChallengeDetailsPresenterImpl(challengeDetailsView, interactor)
    }

    @Test
    fun getDetailsSuccess(){
        val challengeDetailsResponse = ChallengeDetails("test", ApprovedBy("test", "test"),
            "test", CreatedBy("test","test"), "test", "test", emptyList(), "test", "test",
            Rank("test",0,"test"), "test", emptyList(), 0, 0, 0, "test")

        `when`(challengeDetailsService.getChallengeDetails(anyString())).thenReturn(Observable.just(challengeDetailsResponse))

        presenter.getChallengeDetails("122354")

        // Show Challenge Details = Success
        verify(challengeDetailsView).showChallengeDetails(challengeDetailsResponse)
        // Show Error not called
        verify(challengeDetailsView, never()).showError(anyString(), anyBoolean())
    }

    @Test
    fun getDetailsConnectionError() {
        val challengeDetailsResponse = ChallengeDetails("test", ApprovedBy("test", "test"),
            "test", CreatedBy("test","test"), "test", "test", emptyList(), "test", "test",
            Rank("test",0,"test"), "test", emptyList(), 0, 0, 0, "test")

        `when`(challengeDetailsService.getChallengeDetails(anyString())).thenReturn(Observable.error(NetworkErrorException()))

        presenter.getChallengeDetails("122354")

        // View is never called
        verify(challengeDetailsView, never()).showChallengeDetails(challengeDetailsResponse)
        // Show error is called
        verify(challengeDetailsView).showError("Connection Error", true)
    }

    @Test
    fun getDetailsNotFound() {
        val challengeDetailsResponse = ChallengeDetails("test", ApprovedBy("test", "test"),
        "test", CreatedBy("test","test"), "test", "test", emptyList(), "test", "test",
        Rank("test",0,"test"), "test", emptyList(), 0, 0, 0, "test")

        val responseString = "{\"success\":false,\"reason\":\"not found\"}"
        val response = Response.Builder()
            .code(404)
            .message(responseString)
            .request(Request.Builder().url("https://www.codewars.com/api/v1/code-challenges/122354").build())
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
            .addHeader("content-type", "application/json")
            .build()

        val exception = HttpException(retrofit2.Response.error<HttpException>(response.body()!!, response))

        `when`(challengeDetailsService.getChallengeDetails(anyString())).thenReturn(Observable.error(exception))

        presenter.getChallengeDetails("122354")

        // View is never called
        verify(challengeDetailsView, never()).showChallengeDetails(challengeDetailsResponse)
        // Show error is called
        verify(challengeDetailsView).showError("Challenge Details Not Found", false)
    }

    @After
    fun tearDown() {
        reset<Any>(challengeDetailsView)
        reset<Any>(challengeDetailsService)
        validateMockitoUsage()
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}