package carlos.martins.codewars

import android.accounts.NetworkErrorException
import carlos.martins.codewars.challenge.ChallengesInteractorImpl
import carlos.martins.codewars.challenge.ChallengesPresenterImpl
import carlos.martins.codewars.challenge.ChallengesView
import carlos.martins.codewars.challenge.model.AuthoredChallenge
import carlos.martins.codewars.challenge.model.AuthoredChallengeResponse
import carlos.martins.codewars.challenge.model.CompletedChallenge
import carlos.martins.codewars.challenge.model.CompletedChallengeResponse
import carlos.martins.codewars.challenge.service.ChallengesService
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import okhttp3.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import java.util.concurrent.Executor

@RunWith(JUnit4::class)
class ChallengeTest {

    private lateinit var presenter: ChallengesPresenterImpl
    private lateinit var interactor: ChallengesInteractorImpl

    @Mock
    private lateinit var challengeView: ChallengesView
    @Mock
    private lateinit var challengeService: ChallengesService

    @Before
    fun setup() {
        val immediate = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }
        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        MockitoAnnotations.initMocks(this)

        interactor = ChallengesInteractorImpl(challengeService)
        presenter = ChallengesPresenterImpl(challengeView, interactor)
    }

    @Test
    fun getAuthoredChallengeSuccess(){
        val authoredChallengeResponse = AuthoredChallengeResponse(arrayListOf(AuthoredChallenge("", "", emptyList(), "", 1, "", emptyList())))

        `when`(challengeService.getAuthoredChallenges(anyString())).thenReturn(Observable.just(authoredChallengeResponse))

        presenter.getAuthoredChallenges("122354")

        // Show Authored Challenge Details = Success
        verify(challengeView).showAuthoredChallenges(authoredChallengeResponse.listOfAuthoredChallenges)
        // Show Error not called
        verify(challengeView, never()).showError(anyString(), anyBoolean())
    }

    @Test
    fun getCompletedChallengeSuccess(){
        val completedChallengeResponse = CompletedChallengeResponse(1,1, arrayListOf(CompletedChallenge("", emptyList(), "", "", "")))

        `when`(challengeService.getCompletedChallenges(anyString(), anyInt())).thenReturn(Observable.just(completedChallengeResponse))

        presenter.getCompletedChallenges("122354", 0)

        // Show Completed Challenge Details = Success
        verify(challengeView).showCompletedChallenges(completedChallengeResponse.listOfCompletedChallenges, completedChallengeResponse.totalPages)
        // Show Error not called
        verify(challengeView, never()).showError(anyString(), anyBoolean())
    }

    @Test
    fun getAuthoredChallengesConnectionError() {
        val authoredChallengeResponse = AuthoredChallengeResponse(arrayListOf(AuthoredChallenge("", "", emptyList(), "", 1, "", emptyList())))

        `when`(challengeService.getAuthoredChallenges(anyString())).thenReturn(Observable.error(NetworkErrorException()))

        presenter.getAuthoredChallenges("122354")

        // View is never called
        verify(challengeView, never()).showAuthoredChallenges(authoredChallengeResponse.listOfAuthoredChallenges)
        // Show error is called
        verify(challengeView).showError("Connection Error.", true)
    }

    @Test
    fun getCompletedChallengesConnectionError() {
        val completedChallengeResponse = CompletedChallengeResponse(1,1, arrayListOf(CompletedChallenge("", emptyList(), "", "", "")))

        `when`(challengeService.getCompletedChallenges(anyString(), anyInt())).thenReturn(Observable.error(NetworkErrorException()))

        presenter.getCompletedChallenges("122354", 0)

        // View is never called
        verify(challengeView, never()).showCompletedChallenges(completedChallengeResponse.listOfCompletedChallenges, completedChallengeResponse.totalPages)
        // Show error is called
        verify(challengeView).showError("Connection Error.", true)
    }

    @Test
    fun getAuthoredChallengesNotFound() {
        val authoredChallengeResponse = AuthoredChallengeResponse(arrayListOf(AuthoredChallenge("", "", emptyList(), "", 1, "", emptyList())))

        val responseString = "{\"success\":false,\"reason\":\"not found\"}"
        val response = Response.Builder()
            .code(404)
            .message(responseString)
            .request(Request.Builder().url("https://www.codewars.com/api/v1/users/:id_or_username/code-challenges/authored").build())
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
            .addHeader("content-type", "application/json")
            .build()

        val exception = HttpException(retrofit2.Response.error<HttpException>(response.body()!!, response))

        `when`(challengeService.getAuthoredChallenges(anyString())).thenReturn(Observable.error(exception))

        presenter.getAuthoredChallenges("122354")

        // View is never called
        verify(challengeView, never()).showAuthoredChallenges(authoredChallengeResponse.listOfAuthoredChallenges)
        // Show error is called
        verify(challengeView).showError("Challenges Not Found.", false)
    }

    @Test
    fun getCompletedChallengesNotFound() {
        val completedChallengeResponse = CompletedChallengeResponse(1,1, arrayListOf(CompletedChallenge("", emptyList(), "", "", "")))

        val responseString = "{\"success\":false,\"reason\":\"not found\"}"
        val response = Response.Builder()
            .code(404)
            .message(responseString)
            .request(Request.Builder().url("https://www.codewars.com/api/v1/users/:id_or_username/code-challenges/completed?page=0").build())
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
            .addHeader("content-type", "application/json")
            .build()

        val exception = HttpException(retrofit2.Response.error<HttpException>(response.body()!!, response))

        `when`(challengeService.getCompletedChallenges(anyString(), anyInt())).thenReturn(Observable.error(exception))

        presenter.getCompletedChallenges("122354", 0)

        // View is never called
        verify(challengeView, never()).showCompletedChallenges(completedChallengeResponse.listOfCompletedChallenges, completedChallengeResponse.totalPages)
        // Show error is called
        verify(challengeView).showError("Challenges Not Found.", false)
    }

    @After
    fun tearDown() {
        reset<Any>(challengeView)
        reset<Any>(challengeService)
        validateMockitoUsage()
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}