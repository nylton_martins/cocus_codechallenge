package carlos.martins.codewars

import android.accounts.NetworkErrorException
import carlos.martins.codewars.search.SearchInteractor
import carlos.martins.codewars.search.SearchInteractorImpl
import carlos.martins.codewars.search.SearchPresenterImpl
import carlos.martins.codewars.search.SearchView
import carlos.martins.codewars.search.model.User
import carlos.martins.codewars.search.service.CodewarsService
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import okhttp3.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import java.util.*
import java.util.concurrent.Executor

@RunWith(JUnit4::class)
class SearchUserTest {

    private lateinit var presenter: SearchPresenterImpl
    private lateinit var interactor: SearchInteractorImpl

    @Mock
    private lateinit var searchView: SearchView
    @Mock
    private lateinit var codewars: CodewarsService

    @Before
    fun setup() {
        val immediate = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }
        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        MockitoAnnotations.initMocks(this)

        interactor = SearchInteractorImpl(codewars)
        presenter = SearchPresenterImpl(searchView, interactor)
    }

    @Test
    fun getUserConnectionError() {
        val user = User("teste", "teste", emptyList(), Date(), 0)

        `when`(codewars.getUserByIDOrUsername(anyString())).thenReturn(Observable.error(NetworkErrorException()))

        presenter.getUserByIDOrUsername("122354")

        // View is never called
        verify(searchView, never()).showUsers(anyList())
        // Show error is called
        verify(searchView).showError("Connection Error", true)
    }

    @Test
    fun getDetailsNotFound() {
        val user = User("teste", "teste", emptyList(), Date(), 0)


        val responseString = "{\"success\":false,\"reason\":\"not found\"}"
        val response = Response.Builder()
            .code(404)
            .message(responseString)
            .request(Request.Builder().url("https://www.codewars.com/api/v1/users/:id_or_username").build())
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
            .addHeader("content-type", "application/json")
            .build()

        val exception = HttpException(retrofit2.Response.error<HttpException>(response.body()!!, response))

        `when`(codewars.getUserByIDOrUsername(anyString())).thenReturn(Observable.error(exception))

        presenter.getUserByIDOrUsername("122354")

        // View is never called
        verify(searchView, never()).showUsers(anyList())
        // Show error is called
        verify(searchView).showError("User Not Found", false)
    }

    @After
    fun tearDown() {
        reset<Any>(searchView)
        reset<Any>(codewars)
        validateMockitoUsage()
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}